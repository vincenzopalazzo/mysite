import { sveltekit } from '@sveltejs/kit/vite';
import type { UserConfig } from 'vite';
import { svelte } from '@sveltejs/vite-plugin-svelte';

const config: UserConfig = {
	plugins: [sveltekit()]
};

export default config;
